FROM oracle/oraclelinux

RUN chmod a+s /usr/bin/ping \
    `# create oracle user` \
    && groupadd -g 54321 oinstall \
    && groupadd -g 54322 dba \
    && mkdir -p /u01 \
    && useradd -u 54321 oracle -d /u01/home -g oinstall -G dba -m -s /bin/bash \
    `# install packages` \
    && yum -y install \
      `# utilities` \
      perl unzip tar wget nano xterm mc strace \
      `# for oracle 12c1` \
      oracle-rdbms-server-12cR1-preinstall \
      `# sshd` \
      xauth \
      `# server, gateway` \
      csh \
      ksh \
      libXp.so.6 \
      libXtst.so.6 \
      glibc.i686 \
      compat-libstdc++-33.i686 \
      libstdc++ libstdc++.i686 \
      libaio.i686 \
      `# web` \
      gcc \
      make \
      glibc-devel.i686 \
      libstdc++-devel.i686 \
      pcre-devel.i686 \
      zlib-devel.i686 \
      `# to build python psutil` \
      python-devel glibc-devel glibc-headers \
    && yum clean all \
    `# install python psutil` \
    && curl -o /tmp/psutil-master.zip https://codeload.github.com/giampaolo/psutil/zip/master \
    && cd /tmp \
    && unzip psutil-master.zip \
    && cd psutil-master \
    && python setup.py install \
    && rm -rf /tmp/psutil-master*
