#!/bin/bash
set -e
BASE_IMAGE=oracle/oraclelinux
BUILD_IMAGE=oraclelinuxoffline

docker build -t ${REPO_NAME}${BUILD_IMAGE}:latest .